﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetFilesSince
{
    class GetFilesSince
    {
        static void Main(string[] args)
        {
            if (args.Length != 3)
            {
                Console.WriteLine("Please put \"Directory\", search pattern, and how many days back");
                return;
            }
         
            DateTime theDate = DateTime.Today.AddDays(Int32.Parse(args[2]) * -1);

            // Console.WriteLine($"Date {theDate}");

            DirectoryInfo di = new DirectoryInfo(args[0]);

            var filterFiles = from file in di.EnumerateFiles(args[1])
                              where file.LastWriteTime > theDate
                select file;

            foreach (FileInfo fi in filterFiles)
            {
                Console.WriteLine(fi.Name);
            }
        }
    }
}
